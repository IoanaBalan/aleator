﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aleator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Numarul de linii este: ");
            int n = int.Parse(Console.ReadLine());

            Console.WriteLine("Numarul de coloane este: ");
            int m = int.Parse(Console.ReadLine());

            Random rand = new Random();

            int[,] matrice = new int[n, m];
            int [] VectFrecv = new int[10];

            for (int i = 0; i < n; i++)
                for (int j = 0; j < m; j++)
                    matrice[i, j] = rand.Next(1, 10);

            int SumaDiagPrinc = 0, SumaDiagInvers = 0;
            
            if (n <= m)
              {
               for (int i = 0; i < n; i++)
                   {
                    SumaDiagPrinc += matrice[i, i];
                    SumaDiagInvers += matrice[i, m - i - 1];
                    for (int j = 0; j < m; j++)
                         VectFrecv[matrice[i, j]]++;
                    }
                    
               }
            else
            {
               for (int i = 0; i < m; i++)
                    {
                    SumaDiagPrinc += matrice[i, i];
                    SumaDiagInvers += matrice[i, m - i - 1];
                    for (int j = 0; j < n; j++)
                        VectFrecv[matrice[j, i]]++;
                    }
                }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.Write(matrice[i, j] + " ");
                }
                Console.WriteLine();
            }

            Console.WriteLine("Suma de pe diagonala principala este " + SumaDiagPrinc + " iar suma de pe diagonala inversa este " + SumaDiagInvers);

            for (int i = 0; i <= 10; i++)
                Console.WriteLine("Numarul " + i + " se repeta in matrice de " + VectFrecv[i] + " ori.");

            Console.ReadKey();

        }
    }
}
